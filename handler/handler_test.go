package handler

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/danZamarron/restapi"
)

func Test_handlerReturnAllArticles(t *testing.T) {
	testCase := map[string]struct {
		listFnMock     func() ([]*restapi.Article, error)
		wantStatusCode int
		wantResponse   []byte
	}{
		"storage not available should return status code 500": {
			listFnMock: func() ([]*restapi.Article, error) {
				return nil, errors.New("could not exec querry")
			},
			wantStatusCode: http.StatusInternalServerError,
		},
		"should return 500 and message error": {
			listFnMock: func() ([]*restapi.Article, error) {
				return nil, errors.New("could not exec querry")
			},
			wantStatusCode: http.StatusInternalServerError,
			wantResponse:   []byte(`{"message":"something went wrong..."}`),
		},
	}
	for name, tc := range testCase {
		t.Run(name, func(t *testing.T) {
			h := handler{
				store: &StorageMock{
					ListFn: tc.listFnMock,
				},
			}
			w := httptest.NewRecorder()
			req := httptest.NewRequest("GET", "/articles", nil)
			h.ReturnAllArticles(w, req)
			if got := w.Result().StatusCode; got != tc.wantStatusCode {
				t.Fatalf("got %v; want %v status code", got, tc.wantStatusCode)
			}

			if got, want := w.Body.Bytes(), tc.wantResponse; !reflect.DeepEqual(got, want) {
				t.Fatalf("got %v; want %v err response", string(got), string(want))
			}
		})
	}
}

type StorageMock struct {
	ListFn func() ([]*restapi.Article, error)
}

func (s *StorageMock) Save(a *restapi.Article) (*restapi.Article, error) {
	panic("not implemented") // TODO: Implement
}

func (s *StorageMock) Update(a *restapi.Article) (*restapi.Article, error) {
	panic("not implemented") // TODO: Implement
}

func (s *StorageMock) Delete(articleID string) error {
	panic("not implemented") // TODO: Implement
}

func (s *StorageMock) List() ([]*restapi.Article, error) {
	return s.ListFn()
}

func (s *StorageMock) Get(articleID string) (*restapi.Article, error) {
	panic("not implemented") // TODO: Implement
}
