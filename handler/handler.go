package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/danZamarron/restapi"
)

type handler struct {
	store restapi.Storage
}

func New(store restapi.Storage) *handler {
	return &handler{store}
}

func (h *handler) HelloWorld(w http.ResponseWriter, r *http.Request) {
	a, _ := h.store.List()
	w.Write([]byte(fmt.Sprintf("My article is: %v", a[0].Title)))
}

// Status Code
// 200
// Body Response
// {
// 	"articles": [
// 		"id": "12121",
// 		"title": "foobar",
// 		"desc": "loremmmm",
// 		"content": "loremrerjrej"
// 	]
// }
// 500
// {"message": "something went wrog..."}

type errResponse struct {
	Message string `json:"message"`
}

func (h *handler) ReturnAllArticles(w http.ResponseWriter, r *http.Request) {
	_, err := h.store.List()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (h *handler) ReturnSingleArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]
	article, err := h.store.Get(key)
	if err == nil {
		json.NewEncoder(w).Encode(article)
	} else {
		h.jSONError(w, err, 404)
	}
}

func (h *handler) InsertNewArticle(w http.ResponseWriter, r *http.Request) {

	reqBody, err := ioutil.ReadAll(r.Body)
	var article *restapi.Article

	if err != nil {
		fmt.Fprintf(w, "No Data for Creating Register")
	}

	err = json.Unmarshal(reqBody, &article)

	if err != nil {
		fmt.Fprintf(w, "Error Parsing Body")
	}

	article, err = h.store.Save(article)

	if err != nil {
		fmt.Fprintf(w, "Error Saving")
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(article)
}

func (h *handler) UpdateArticle(w http.ResponseWriter, r *http.Request) {
	var updatedArticle *restapi.Article
	_ = mux.Vars(r)

	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		fmt.Fprintf(w, "No Data for Updating Register")
	}

	err = json.Unmarshal(reqBody, &updatedArticle)

	if err != nil {
		fmt.Fprintf(w, "Error Parsing Body")
	}

	updatedArticle, err = h.store.Update(updatedArticle)

	if err == nil {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(updatedArticle)
	} else {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(err)
	}

}

func (h *handler) DeleteArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]
	err := h.store.Delete(key)
	if err == nil {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(err)
	}
}

func (h *handler) jSONError(w http.ResponseWriter, err interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(err)
}
