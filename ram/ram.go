package ram

import (
	"errors"

	"gitlab.com/danZamarron/restapi"
)

type ram struct {
	storage []*restapi.Article
}

func New() *ram {
	return &ram{
		storage: []*restapi.Article{
			&restapi.Article{
				ID:      "1",
				Title:   "Foo bar",
				Desc:    "Desc",
				Content: "Content",
			},
			&restapi.Article{
				ID:      "2",
				Title:   "Bar Foo",
				Desc:    "123",
				Content: "321",
			},
			&restapi.Article{
				ID:      "3",
				Title:   "Foo Fighters",
				Desc:    "Foo",
				Content: "Bar",
			},
		},
	}
}

func (r *ram) List() ([]*restapi.Article, error) {
	return r.storage, nil
}

func (r *ram) Get(articleID string) (*restapi.Article, error) {

	for _, article := range r.storage {
		if article.ID == articleID {
			return article, nil
		}
	}

	return nil, errors.New("id not found")
}

func (r *ram) Save(a *restapi.Article) (*restapi.Article, error) {

	r.storage = append(r.storage, a)
	return a, nil
}

func (r *ram) Update(a *restapi.Article) (*restapi.Article, error) {

	for index, article := range r.storage {
		if article.ID == a.ID {
			article = a
			r.storage[index] = article
			return article, nil
		}
	}

	return nil, errors.New("Item not Found")

}

func (r *ram) Delete(articleID string) error {
	for index, article := range r.storage {
		if article.ID == articleID {
			r.storage = append(r.storage[:index], r.storage[index+1:]...)
			return nil
		}
	}

	return errors.New("Item not Found")
}
