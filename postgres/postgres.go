package postgres

import "gitlab.com/danZamarron/restapi"

type postgres struct{}

func New() *postgres {
	return &postgres{}
}

func (p *postgres) Save(a *restapi.Article) error {
	panic("not implemented") // TODO: Implement
}

func (p *postgres) Update(a *restapi.Article) error {
	panic("not implemented") // TODO: Implement
}

func (p *postgres) Delete(articleID string) error {
	panic("not implemented") // TODO: Implement
}

func (p *postgres) List() ([]*restapi.Article, error) {
	panic("not implemented") // TODO: Implement
}

func (p *postgres) Get(articleID string) (*restapi.Article, error) {
	panic("not implemented") // TODO: Implement
}
