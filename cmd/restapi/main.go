package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/danZamarron/restapi/handler"
	"gitlab.com/danZamarron/restapi/ram"
)

func main() {
	router := mux.NewRouter()
	h := handler.New(ram.New())

	router.HandleFunc("/hello", h.HelloWorld).Methods(http.MethodGet)
	router.HandleFunc("/articles", h.ReturnAllArticles).Methods(http.MethodGet)
	router.HandleFunc("/article/{id}", h.ReturnSingleArticle).Methods(http.MethodGet)
	router.HandleFunc("/article", h.InsertNewArticle).Methods(http.MethodPost)
	router.HandleFunc("/article/{id}", h.UpdateArticle).Methods(http.MethodPut)
	router.HandleFunc("/article/{id}", h.DeleteArticle).Methods(http.MethodDelete)
	router.Handle("/asdg", errorHandler(HelloWorldX))

	log.Fatal(http.ListenAndServe(":8080", router))
}

type errorHandler func(w http.ResponseWriter, r *http.Request) error

func (fn errorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := fn(w, r)
	if err == nil {
		return
	}

}

func HelloWorldX(w http.ResponseWriter, r *http.Request) error {
	w.Write([]byte(fmt.Sprintf("Hello")))
	return nil
}
