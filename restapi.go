package restapi

type Article struct {
	ID      string `json:"id"`
	Title   string `json:"title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

type Storage interface {
	Save(a *Article) (*Article, error)
	Update(a *Article) (*Article, error)
	Delete(articleID string) error
	List() ([]*Article, error)
	Get(articleID string) (*Article, error)
}
